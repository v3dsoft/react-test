import React, { useState } from 'react';

import { RestaurantInfo } from "../../components/RestList/hooks/restraurants";

import {
    MainBody
} from "./ListPage.styled";
import { Header, RestList, RestDetails } from '../../components';

export const ListPage = () => {
    const [ currItem, setCurrItem ] = useState<RestaurantInfo | null>(null);

    const handleItemSelect = (item: RestaurantInfo) => {
        setCurrItem(item);
    };

    const handleBack = () => {
        setCurrItem(null);
    };

    return (
        <MainBody>
            <Header title="Lunch Tyme" onBack={currItem ? handleBack : undefined} />
            { currItem ? <RestDetails data={currItem} /> : <RestList onSelect={handleItemSelect} /> }
        </MainBody>
    );
}
