import styled from 'styled-components';

export const HeaderBody = styled.div`
    background: #43e895;
    color: white;
    
    padding: 40px 15px 15px 15px;
    
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

export const BackBtn = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    
    min-width: 60px;
    min-height: 60px; 

    cursor: pointer;
`;

export const HeaderText = styled.div`
    flex: 1;
    
    display: flex;
    flex-direction: row;
    justify-content: center;
    
    font-size: 17px;
    font-weight: bold;
`;
