import React from 'react';

import { HeaderBody, BackBtn, HeaderText } from "./Header.styled";

export interface HeaderProps {
    title: string;
    onBack?: () => void;
}

export const Header = (props: HeaderProps) => (
    <HeaderBody>
        { props.onBack && (
            <BackBtn onClick={props.onBack}>
                <img src="icon_back.png" alt="" />
            </BackBtn>
        )}
        <HeaderText>{props.title}</HeaderText>
        <img src="icon_map.png" alt="" />
    </HeaderBody>
);
