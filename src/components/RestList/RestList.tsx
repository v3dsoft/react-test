import React from 'react';

import { RestListBody } from "./RestList.styled";
import RestListItem from "./RestListItem";

import { useRestaurants, RestaurantInfo } from "./hooks/restraurants";

export interface RestListProps {
    onSelect?: (item: RestaurantInfo) => void;
}

export const RestList = (props: RestListProps) => {
    const { restList } = useRestaurants();

    return (
        <RestListBody>
            { restList.map((item, index) => <RestListItem key={`rlItem_${index}`} data={item} onSelect={props.onSelect} />) }
        </RestListBody>
    );
}
