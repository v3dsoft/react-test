import styled from 'styled-components';

export const RestListBody = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    
    @media screen and (min-width: 768px) {
        flex-direction: row;
        flex-wrap: wrap;
        align-items: flex-start;;
    }
`;

export interface RestListItemBodyProps {
    bgImage: string;
}

export const RestListItemBody = styled.div<RestListItemBodyProps>`
    background-image: url("${props => props.bgImage}");
    background-position: center;
    background-size: cover;
    
    cursor: pointer;
    
    @media screen and (min-width: 768px) {
        width: 50%
    }
`;

export const RestListItemGrad = styled.div`
    position: relative;
    
    width: 100%;
    height: 180px;
    
    background-image: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.7));
`;

export const RestNameBox = styled.div`
    position: absolute;
    left: 12px;
    bottom: 6px;
    
    color: white;
`;

export const RestNameName = styled.div`
    font-size: 16px;
    font-weight: bold;
`;

export const RestNameCategory = styled.div`
    font-size: 12px;
    font-weight: bold;
`;
