import React from 'react';

import { RestaurantInfo } from "./hooks/restraurants";

import {
    RestListItemBody,
    RestListItemGrad,
    RestNameBox,
    RestNameName,
    RestNameCategory
} from "./RestList.styled";

export interface RestListItemProps {
    data: RestaurantInfo;
    onSelect?: (item: RestaurantInfo) => void;
};

const RestListItem = (props: RestListItemProps) => {
    const handleSelect = () => {
        props.onSelect && props.onSelect(props.data)
    }

    return (
        <RestListItemBody bgImage={props.data.bgImage} onClick={handleSelect}>
            <RestListItemGrad>
                <RestNameBox>
                    <RestNameName>{props.data.name}</RestNameName>
                    <RestNameCategory>{props.data.category}</RestNameCategory>
                </RestNameBox>
            </RestListItemGrad>
        </RestListItemBody>
    );
};

export default RestListItem;
