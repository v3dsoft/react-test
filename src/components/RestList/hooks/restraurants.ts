import { useState, useEffect } from 'react';

const REST_LIST_URL = 'http://sandbox.bottlerocketapps.com/BR_iOS_CodingExam_2015_Server/restaurants.json';

export interface RestaurantInfo {
    name: string;
    category: string;

    bgImage: string;

    address: string;
    phone: string;
    twitter: string;

    mapLat: number;
    mapLng: number;
}

export const useRestaurants = () => {
    const [ restList, setRestList ] = useState<RestaurantInfo[]>([]);

    useEffect(() => {
        fetch(REST_LIST_URL)
            .then(res => res.json())
            .then(data => {
                if (data.restaurants) {
                    const newRestList = data.restaurants.map((item: any) => ({
                        name: item.name,
                        category: item.category,

                        bgImage: item.backgroundImageURL,

                        address: item.location.address,
                        phone: item.contact ? item.contact.formattedPhone : '',
                        twitter: item.contact ? item.contact.twitter : '',

                        mapLat: parseFloat(item.location.lat),
                        mapLng: parseFloat(item.location.lng)
                    }));

                    setRestList(newRestList);
                }
            })
    }, [setRestList])

    return { restList };
}
