import React from 'react';
import GoogleMapReact from 'google-map-react';

import { RestaurantInfo } from "../RestList/hooks/restraurants";

import {
    RestDetailsBody,
    MapContainer,
    MapMarker,
    MapPlaceholder,
    RestNameBlockOuter,
    RestNameBlockInner,
    RestDetailsName,
    RestDetailsCategory,
    RestDetailsContact
} from "./RestDetails.styled";

export interface RestDetailsProps {
    data: RestaurantInfo;
}

export const RestDetails = (props: RestDetailsProps) => {
    const { data } = props;

    const mapKey = process.env.REACT_APP_GOOGLE_MAP_KEY;

    return (
        <RestDetailsBody>
            <MapContainer>
                { mapKey ? (
                    <GoogleMapReact
                        bootstrapURLKeys={{key: process.env.REACT_APP_GOOGLE_MAP_KEY ?? ''}}
                        defaultZoom={11}
                        defaultCenter={{ lat: data.mapLat, lng: data.mapLng }}
                    >
                        <MapMarker
                            lat={data.mapLat}
                            lng={data.mapLng}
                            text={data.name}
                        />
                    </GoogleMapReact>
                ) : (
                    <MapPlaceholder>Please create environment variable REACT_APP_GOOGLE_MAP_KEY with map API key</MapPlaceholder>
                ) }
            </MapContainer>
            <RestNameBlockOuter>
                <RestNameBlockInner>
                    <RestDetailsName>{data.name}</RestDetailsName>
                    <RestDetailsCategory>{data.category}</RestDetailsCategory>
                </RestNameBlockInner>
            </RestNameBlockOuter>
            <RestDetailsContact>{data.address}</RestDetailsContact>
            <RestDetailsContact>{data.phone}</RestDetailsContact>
            <RestDetailsContact>{data.twitter}</RestDetailsContact>
        </RestDetailsBody>
    );
};
