import styled from 'styled-components';

export const RestDetailsBody = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
`;

export const MapContainer = styled.div`
    height: 180px;
    
    @media screen and (min-width: 768px) {
        height: 40vh;
    }
`;

export interface MapMarkerProps {
    lat: number;
    lng: number;

    text: string;
}

export const MapMarker = styled.div<MapMarkerProps>`
    width: 16px;
    height: 16px;
    
    background-color: #34B379;
    
    border-radius: 8px;
`;

export const MapPlaceholder = styled.div`
    width: 100%;
    height: 100%;
    
    display: flex;
    justify-content: center;
    align-items: center;
    
    color: red;
    font-size: 16px;
`;

export const RestNameBlockOuter = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    
    height: 60px;
    padding: 0 12px;
    
    background-color: #34B379;
`;

export const RestNameBlockInner = styled.div`
    color: white;
`;

export const RestDetailsName = styled.div`
    font-size: 16px;
    font-weight: bold;
`;

export const RestDetailsCategory = styled.div`
    font-size: 12px;
    
    margin-top: 6px;
`;

export const RestDetailsContact = styled.div`
    padding: 16px 12px 10px 12px;
    
    color: #2A2A2A;
    font-size: 16px;
`;
