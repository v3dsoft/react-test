import styled from 'styled-components';

export const MainBody = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
`;
